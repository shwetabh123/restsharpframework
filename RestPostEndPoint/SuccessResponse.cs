﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpFramework.RestPostEndPoint
{

    [TestClass]
    public class SuccessResponse
    {
        // Succes code of response received
        public string SuccessCode;
        // Message of response received
        public string Message;

        private static Random random = new Random();

        public static string RandomString(int length)
        {
           // const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }



}
