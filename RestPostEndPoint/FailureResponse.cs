﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpFramework.RestPostEndPoint
{

    [TestClass]
    public class FailureResponse
    {
        // FaultId of response received
        public string FaultId;
        // Fault of response received
        public string fault;
    }
}
