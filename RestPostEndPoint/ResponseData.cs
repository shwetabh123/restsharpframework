﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpFramework.RestPostEndPoint
{

    [TestClass]
    public class ResponseData
    {
        // Succes code of response received
        public string SuccessCode;
        // Message of response received
        public string Message;
    }
}
