﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpFramework.RestPostEndPoint
{

    [TestClass]
    public class TestPostEndPoint
    {


        private string geturl = "https://reqres.in/";

        private string geturl1 = "http://ergast.com/api/f1/";



        [TestMethod]

        public void TestPostUsingRestSharp()
        {

            //  https://reqres.in/



            string jsondata = "{" +

                "\"name\":\"morpheus\"," +
    "\"job\": \"leader\"" +
      "}";




            IRestClient restClient = new RestClient(geturl);

            IRestRequest restRequest = new RestRequest("api/users", Method.POST);




            restRequest.AddHeader("Content-Type", "application/json");




            restRequest.AddHeader("Accept", "application/json");

            restRequest.AddJsonBody(jsondata);


            IRestResponse restResponse = restClient.Post(restRequest);


            Console.WriteLine(restResponse.StatusCode);

            Console.WriteLine(restResponse.Content);

       
            JObject o = JObject.Parse(restResponse.Content);


            string id = (string)o.SelectToken("id");

            Console.WriteLine(id);
        }


        [TestMethod]

        public void TestPostUsingRestSharpDynamicstaticpayload()
        {

            IRestClient restClient = new RestClient("http://restapi.demoqa.com/customer");


        
            string Fname= SuccessResponse.RandomString(10);

            string LastName = SuccessResponse.RandomString(10);


           string UserName = SuccessResponse.RandomString(10);

            string Password = SuccessResponse.RandomString(10);

            string Email1 = SuccessResponse.RandomString(10);

            string Email =  String.Concat(Email1, "@gmail.com");



            //Static payload

            //Creating Json object

            //JObject jObjectbody = new JObject();


            //jObjectbody.Add("FirstName", "Narayn23");
            //jObjectbody.Add("LastName", "Kaluri123");
            //jObjectbody.Add("UserName", "NaraynKasdfsdfluri32");
            //jObjectbody.Add("Password", "Passwovasdfsdaf21233");
            //jObjectbody.Add("Email", "abcdafsad23@hotmail.com");




            //Dynamicpayload
            string jsondata = "{\r\n\"FirstName\" : \""+Fname+"\",\r\n\r\n\"LastName\" : \""+ LastName + "\",\r\n\r\n\"UserName\" : \""+UserName+"\",\r\n\r\n\"Password\" : \""+ Password + "\",\r\n\r\n\"Email\" : \""+Email+"\"\r\n\r\n}";






           IRestRequest restRequest = new RestRequest("/register", Method.POST);


            restRequest.AddHeader("Content-Type", "application/json");


            restRequest.AddHeader("Accept", "application/json");


            //Adding Json body as parameter to the post request

            restRequest.AddJsonBody(jsondata);


        //    restRequest.AddParameter("application/json", jObjectbody, ParameterType.RequestBody);

            IRestResponse restResponse = restClient.Execute(restRequest);

            ResponseData responseData = SimpleJson.DeserializeObject<ResponseData>(restResponse.Content);

          //  Console.WriteLine(responseData.Message);


         //   Console.WriteLine(responseData.SuccessCode);


      
            if ((int)restResponse.StatusCode== 200)
            {

                // Deserialize the Response body into RegistrationFailureResponse 
                FailureResponse responseBody = SimpleJson.DeserializeObject<FailureResponse>(restResponse.Content);

                // Use the FailureResponse class instance to Assert the values of Response.

                Assert.AreEqual("User already exists", responseBody.FaultId);
                Assert.AreEqual("FAULT_USER_ALREADY_EXISTS", responseBody.fault);
            }

            else if ((int)restResponse.StatusCode == 201)
            {
                // Deserialize the Response body into SuccessResponse
                SuccessResponse responseBody = SimpleJson.DeserializeObject<SuccessResponse>(restResponse.Content);

                // Use the SuccessResponse class instance to Assert the values of Response.		
                Assert.AreEqual("OPERATION_SUCCESS", responseBody.SuccessCode);
                Assert.AreEqual("Operation completed successfully", responseBody.Message);
            }


        }




        [TestMethod]
        public void AuthenticationBasics()
        {
            RestClient restClient = new RestClient();

            restClient.BaseUrl = new Uri("http://restapi.demoqa.com/authentication/CheckForAuthentication");

            RestRequest restRequest = new RestRequest(Method.GET);

            restClient.Authenticator = new HttpBasicAuthenticator("ToolsQA", "TestPassword");

            IRestResponse restResponse = restClient.Execute(restRequest);


            Console.WriteLine("Status code: " + (int)restResponse.StatusCode);


            Console.WriteLine("Status message " + restResponse.Content);
        }


        [TestMethod]
        public void AuthenticationBasicsnew()
        {

            string url = "https://myurl.com";
            string client_id = "client_id";
            string client_secret = "client_secret";
            //request token
            var restclient = new RestClient(url);
            RestRequest request = new RestRequest("request/oauth") { Method = Method.POST };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("client_id", client_id);
            request.AddParameter("client_secret", client_secret);
            request.AddParameter("grant_type", "client_credentials");
            var tResponse = restclient.Execute(request);
            var responseJson = tResponse.Content;
            var token = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseJson)["access_token"].ToString();

            Console.WriteLine(token.Length);

            Console.WriteLine(token);

          //  return token.Length > 0 ? token : null;


        }


        }
    }