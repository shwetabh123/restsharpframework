﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serialization.Json;
using RestSharp.Serialization.Xml;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpFramework.RestGetEndPoint
{

    [TestClass]
    public class TestGetEndPoint
    {

        // private string geturl = "http://localhost:8080/laptop-bag/webapi/api/all";



       private string geturl = "https://reqres.in/";

        private string geturl1 = "http://ergast.com/api/f1/";



        //working

        [TestMethod]

        public void TestGetUsingRestSharp()
        {

            //private string geturl = "https://reqres.in/";

      


        IRestClient restClient = new RestClient();

            IRestRequest restRequest = new RestRequest(geturl);


            IRestResponse restResponse = restClient.Get(restRequest);

            Console.WriteLine(restResponse.IsSuccessful);//TRUE
            Console.WriteLine(restResponse.StatusCode);//OK

            Console.WriteLine(restResponse.ErrorMessage);
            Console.WriteLine(restResponse.ErrorException);



            if (restResponse.IsSuccessful)
            {
                Console.WriteLine("StatusCode IS:-" + restResponse.StatusCode);//OK

                Console.WriteLine("RESPONSE CONTENT" + restResponse.Content);


            }


        }


        [TestMethod]

        public void TestGetwithJson_Deserialize()
        {


           //      http://ergast.com/api/f1/



        //IRestClient restClient = new RestClient();

        //IRestRequest restRequest = new RestRequest(geturl);


        IRestClient restClient = new RestClient(geturl1);

            IRestRequest restRequest = new RestRequest("2016/circuits.json", Method.GET);


            restRequest.AddHeader("Accept", "application/json");

            IRestResponse restResponse = restClient.Execute(restRequest);





            //**********************************



            // //Serialization:

            //          // Convert an object to JSON string format


            //          string jsonData = JsonConvert.SerializeObject(obj);

            //          Response.Write(jsonData);
            //          Deserialization::


            //**********************************


            //       //   To deserialize a dynamic object

            // Convert  JSON string format to an object 


            //string json = @"{
            //    'Name': 'name',
            //    'Description': 'des'
            //  }";

            //          var res = JsonConvert.DeserializeObject<dynamic>(json);

            //          Response.Write(res.Name);



            //**********************************


            //not working

            //var deserialize = new JsonDeserializer();

            //var jsonResponse = deserialize.Deserialize<Dictionary<string, string>>(restResponse);


            //var jsonObject = jsonResponse["MRData.CircuitTable"];


            //****************************************



            //   To deserialize a dynamic object

            // Convert  JSON string format to an object 






            var jsonResponse = JsonConvert.DeserializeObject<dynamic>(restResponse.Content);

            var jsonObject = jsonResponse.MRData.CircuitTable;

            
           //  System.Console.WriteLine(jsonObject);//working

              int seasonname = (int)jsonObject.season;


            System.Console.WriteLine(seasonname);//working --2016


//**********************************************

            JObject rss = JObject.Parse(restResponse.Content);


            var postcircuitId =
                from p in rss["MRData"]["CircuitTable"]["Circuits"]
                select (string)p["circuitId"];


            //all circuitid

            foreach (var item in postcircuitId)
            {
                Console.WriteLine(item);
            }

            //output

            //            albert_park
            //americas
            //bahrain
            //BAK
            //catalunya
            //hockenheimring
            //hungaroring
            //interlagos
            //marina_bay
            //monaco
            //monza
            //red_bull_ring
            //rodriguez
            //sepang
            //shanghai
            //silverstone
            //sochi
            //spa
            //suzuka
            //villeneuve
            //yas_marina

//******************************

            JObject rs = JObject.Parse(restResponse.Content);


            var postLocation =
                from p in rss["MRData"]["CircuitTable"]["Circuits"]
                select (object)p["Location"];


            //All locations

            foreach (var item in postLocation)
            {
            
                
                Console.WriteLine(item);

              

            }





            JObject o = JObject.Parse(restResponse.Content);


            string firstcountry = (string)o.SelectToken("MRData.CircuitTable.Circuits[0].Location.country");

            //first country

            Console.WriteLine(firstcountry);






            JObject o1 = JObject.Parse(restResponse.Content);


            object Locationone = (object)o1.SelectToken("MRData.CircuitTable.Circuits[0].Location");

            //first location

            Console.WriteLine(Locationone);

        }











        [TestMethod]

        public void TestGetwithXml_Deserialize()
        {
            //      http://ergast.com/api/f1/




            IRestClient restClient = new RestClient(geturl1);

            IRestRequest restRequest = new RestRequest("2016/circuits.json", Method.GET);


            restRequest.AddHeader("Accept", "application/xml");



            IRestResponse restResponse = restClient.Execute(restRequest);


            //   To deserialize a dynamic object

            // Convert  XML string format to an object 


            var dotNetXmlDeserializer = new RestSharp.Deserializers.DotNetXmlDeserializer();

            System.Console.WriteLine(restResponse.StatusCode);//working


            var xmlResponse = dotNetXmlDeserializer.Deserialize<dynamic>(restResponse);


            var xmlObject = xmlResponse.MRData.CircuitTable;


            System.Console.WriteLine(xmlObject);//working

            int seasonname = (int)xmlObject.season;


            System.Console.WriteLine(seasonname);//working --2016




        }


    }
    }